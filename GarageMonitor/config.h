// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. 

// Interval time(ms) for sending message to IoT Hub
#define INTERVAL 30000

#define MESSAGE_MAX_LEN 256

#define DEVICE_ID "AZ3166_Garage"

#define TEMPERATURE_ALERT_LOW 10
#define TEMPERATURE_ALERT_HIGH 25